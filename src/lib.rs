mod renderer;
mod html_target;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

use renderer::Vector;
use renderer::Renderer;
use html_target::HTMLTarget;
use web_sys::{HtmlCanvasElement, Node, Window, CanvasRenderingContext2d, console};

use std::f64::consts::PI;
use js_sys::Array;


#[wasm_bindgen]
extern {
    pub fn alert(s: &str);
}

#[wasm_bindgen]
pub fn hello_world(name: &str) {
    alert(&format!("Hello, {}!", name));

    let a = Vector { x: 10.3, y: 0.4, z: 2.3 };
    alert(&format!("({}, {}, {})", a.x, a.y, a.z));

    let b = Vector { x: 1.0, y: 2.0, z: 3.0 };
    alert(&format!("({}, {}, {})", b.x, b.y, b.z));

    let c = a + b;
    alert(&format!("({}, {}, {})", c.x, c.y, c.z));
}

#[wasm_bindgen]
pub fn run() {
    draw_frame();
}

pub fn draw_frame() {
    let renderer = Renderer::new();
    let mut target = HTMLTarget::from_id("eothel-canvas", 5);
    renderer.draw(&mut target);
}