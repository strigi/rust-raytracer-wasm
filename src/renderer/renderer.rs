use std::f32::consts::PI;
use super::*;

pub struct Renderer {
    pub scene: Scene,
    pub camera: Camera,
}

impl Renderer {
    pub fn new() -> Self {
        return Renderer {
            scene: Scene::new(),
            camera: Camera::new(Point::new(0.0, 0.0, 0.0), Size::new(16.0, 9.0), 50.0),
        };
    }

    pub fn draw(&self, target: &mut dyn Target) {
        // println!("BEGIN DRAW");
        target.clear();

        self.camera.capture(&self.scene, target);

        target.flip();
        // println!("END DRAW");
    }
}