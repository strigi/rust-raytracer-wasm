use super::*;

pub struct Light {
    pub position: Point,
    pub color: Color,
}

impl Light {
    pub fn new(position: Point, color: Color) -> Self {
        return Self {
            position,
            color,
        };
    }
}