use super::*;

#[derive(Copy, Clone)]
pub struct Plane {
    pub a: Point,
    pub b: Point,
    pub c: Point,
}

impl Plane {
    fn normal(&self) -> Vector {
        return Vector::between_points(self.a, self.b).cross(Vector::between_points(self.a, self.c)).normalize();
    }
}

#[derive(Copy, Clone)]
pub struct Rectangle {
    plane: Plane
}

impl Rectangle {
    pub fn new(bottom_left: Point, bottom_right: Point, top_left: Point) -> Self {
        // TODO: vectors ab and ac must be perpendicular

        Self {
            plane: Plane { a: bottom_left, b: bottom_right, c: top_left }
        }
    }

    pub fn width_vector(&self) -> Vector {
        return Vector::between_points(self.plane.a, self.plane.b);
    }

    pub fn height_vector(&self) -> Vector {
        return Vector::between_points(self.plane.a, self.plane.c);
    }

    pub fn width(&self) -> f32 {
        return self.width_vector().length();
    }

    pub fn height(&self) -> f32 {
        return self.height_vector().length();
    }

    pub fn bottom_left(&self) -> Point {
        return self.plane.a;
    }

    pub fn bottom_right(&self) -> Point {
        return self.plane.b;
    }

    pub fn top_left(&self) -> Point {
        return self.plane.c;
    }

    pub fn top_right(&self) -> Point {
        return self.plane.a + self.width_vector() + self.height_vector();
    }

    pub fn normal(&self) -> Vector {
        return self.plane.normal();
    }
}
