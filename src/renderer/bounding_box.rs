use super::*;

// Axis Aligned Bounding Box aka AABB.
// The bounding volume is determined by two points `a` and `b` where `a` must be the smaller of the two
// since this is an axis aligned bounding rectangle, the `x`, `y`, `z` of `a` are always respectively less than those of `b`.
pub struct BoundingBox {
    a: Point,
    b: Point,
    pub color: Color
}

impl BoundingBox {
    pub fn new(a: Point, b: Point, color: Color) -> Self {
        assert!(a.x < b.x && a.y < b.y && a.z < b.z);
        return Self { a, b, color }
    }

    // Brainless port: https://tavianator.com/2011/ray_box.html
    pub fn intersection(&self, ray: &Line) -> bool {
        let n = ray.direction();
        let n_inv = Vector::new(1.0 / n.x, 1.0 / n.y, 1.0 / n.z);

        let tx1 = (self.a.x - ray.a.x) * n_inv.x;
        let tx2 = (self.b.x - ray.a.x) * n_inv.x;

        let mut tmin = tx1.min(tx2);
        let mut tmax = tx1.max(tx2);

        let ty1 = (self.a.y - ray.a.y) * n_inv.y;
        let ty2 = (self.b.y - ray.a.y) * n_inv.y;

        tmin = tmin.max(ty1.min(ty2));
        tmax = tmax.min(ty1.max(ty2));

        let tz1 = (self.a.z - ray.a.z) * n_inv.z;
        let tz2 = (self.b.z - ray.a.z) * n_inv.z;

        tmin = tmin.max(tz1.min(tz2));
        tmax = tmax.min(tz1.max(tz2));

        return tmax >= tmin;
    }
}

fn max(a: f32, b: f32) -> f32 {
    return if a > b { a } else { b }
}

fn min(a: f32, b: f32) -> f32 {
    return if a < b { a } else { b }
}