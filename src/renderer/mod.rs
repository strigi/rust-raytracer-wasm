mod color;
mod vector;
mod renderer;
mod target;
mod size;
mod scene;
mod triangle;
mod sphere;
mod line;
mod camera;
mod bounding_box;
mod plane;
mod light;

// TODO: How to work with unit tests?
mod vector_spec;
mod color_spec;

pub use color::Color;
pub use vector::{Vector, Point};
pub use renderer::Renderer;
pub use target::Target;
pub use target::Key;
pub use size::Size;
pub use scene::Scene;
pub use sphere::Sphere;
pub use line::Line;
pub use camera::Camera;
pub use bounding_box::BoundingBox;
pub use plane::Plane;
pub use light::Light;