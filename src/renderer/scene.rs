use super::*;

pub struct Scene {
    pub spheres: Vec<Sphere>,
    pub voxel: BoundingBox,
    pub light: Light,
}

impl Scene {
    // TODO: allow loading/building scenes non-hardcoded
    pub fn new() -> Self {
        return Self {
            // TODO: merge sphere and voxels into abstract hierarchy of objects.
            spheres: vec![
                Sphere::new(Point::new(-2.0, 0.0, 2.0), 2.0, Color::RED),
                Sphere::new(Point::new(2.0, 0.0, 2.0), 2.0, Color::BLUE),
            ],
            voxel: BoundingBox::new(Point::new(-1.0, -1.0, 4.0), Point::new(1.0, 1.0, 6.0), Color::RED / 4.0),
            light: Light::new(Point::new(0.0, 1.0, 0.0), Color::GREEN),
        }
    }
}
