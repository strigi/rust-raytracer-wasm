use super::*;

pub struct Sphere {
    pub center: Point,
    pub radius: f32,
    pub color: Color
}

impl Sphere {
    pub fn new(center: Point, radius: f32, color: Color) -> Self {
        return Sphere {
            center,
            radius,
            color
        };
    }

    // TODO: return t and or intersection point
    // TODO: understand the mathematics better and clean up code
    // https://fiftylinesofcode.com/ray-sphere-intersection/
    // Parametric sphere equation: 0 = |P - C| - r
    // Parametric line equation: P = P0 + t*V
    // r: sphere radius (scalar)
    // C: sphere center (Point)
    // P: point on surface of sphere or on line
    // V: direction of the line/ray (from point a -> b), must be normalized
    // t: parametric scalar value of the line (e.g. how many times the distance between a and b the intersection is on the line (or is this in normalized units due to V being normalized?
    fn intersection_t(&self, ray: &Line) -> Option<f32> {
        let c = self.center;
        let r = self.radius;
        let o = ray.a;
        let d = ray.direction();

        let co = o - c;
        let p = d.dot(co);
        let q = co.dot(co) - r * r;
        let discriminant = p * p - q;

        if discriminant < 0.0 {
            // No intersection (line misses sphere)
            return None;
        } else if discriminant == 0.0 { // TODO: close to epsilon?
            // One intersection (line is tangent/grazes to surface of sphere)
             //let root = discriminant.sqrt();
            let t = -p;

            return Some(t_over_ab(t, ray));
        } else {
            // Two intersections (lines goes through sphere
            let root = discriminant.sqrt();
            let t1 = -p - root;
            let t2 = -p + root;
            let tmin = t1.min(t2);
            let tmax = t1.max(t2);

            return Some(t_over_ab(tmin, ray));
        }

        fn t_over_ab(t_over_normalized_direction: f32, ray: &Line) -> f32 {
            let length_ab = Vector::between_points(ray.a, ray.b).length();
            let t_over_ab = t_over_normalized_direction / length_ab;
            return t_over_ab;
        }
    }

    pub fn intersection(&self, ray: &Line) -> Option<(f32, Point, Vector)> {
        let t_option = self.intersection_t(ray);
        if t_option.is_none() {
            return None;
        }
        let t = t_option.unwrap();

        let intersection_point = ray.point_on(t);
        let normal = Vector::between_points(self.center, intersection_point).normalize();

        return Some((t, intersection_point, normal));
    }
}