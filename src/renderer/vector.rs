use std::fmt::{Display, Formatter, Result};
use std::ops::{Add, Sub, Mul, Div};

#[derive(Copy, Clone)]
pub struct Vector {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vector {
    pub const ZERO: Vector = Vector::new(0.0, 0.0, 0.0);
    pub const ORIGIN: Point = Vector::ZERO;

    pub const X_AXIS: Vector = Vector::new(1.0, 0.0, 0.0);
    pub const Y_AXIS: Vector = Vector::new(0.0, 1.0, 0.0);
    pub const Z_AXIS: Vector = Vector::new(0.0, 0.0, 1.0);

    pub const fn new(x: f32, y: f32, z: f32) -> Self {
        return Vector { x, y, z };
    }

    /// Creates a vector between two points. The vector direction will be from a to b
    /// This is really just a semantic version of `c = b - a` but is easier to understand as such.
    pub fn between_points(a: Point, b: Point) -> Self {
        return b - a;
    }

    pub fn dot(self, rhs: Self) -> f32 {
        return self.x * rhs.x + self.y * rhs.y + self.z * rhs.z;
    }

    pub fn cross(self, rhs: Self) -> Self {
        return Vector {
            x: self.y * rhs.z - self.z * rhs.y,
            y: self.z * rhs.x - self.x * rhs.z,
            z: self.x * rhs.y - self.y * rhs.x
        };
    }

    pub fn length(self) -> f32 {
        //return (self.x * self.x + self.y * self.y + self.z * self.z).sqrt();
        return self.dot(self).sqrt();
    }

    pub fn normalize(&self) -> Vector {
        return *self / self.length();
    }

    pub fn angle(&self, other: Vector) -> f32 {
        return (self.dot(other) / (self.length() * other.length())).acos();
    }
}

pub type Point = Vector;

impl Add<Self> for Vector {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Vector {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl Sub<Self> for Vector {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        Vector {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl Mul<f32> for Vector {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output {
        Vector {
            x: self.x * rhs,
            y: self.y * rhs,
            z: self.z * rhs,
        }
    }
}

impl Mul<Self> for Vector {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        return self.cross(rhs);
    }
}

impl Div<f32> for Vector {
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output {
        return Vector {
            x: self.x / rhs,
            y: self.y / rhs,
            z: self.z / rhs
        }
    }
}

impl Display for Vector {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "({:.3}, {:.3}, {:.3})", self.x, self.y, self.z)
    }
}
