use std::fmt::{Display, Formatter, Result};
use std::ops::{Add, Mul, Div};

#[derive(Copy, Clone)]
pub struct Color {
    pub red: f32,
    pub green: f32,
    pub blue: f32
}

impl Color {
    pub const BLACK: Color = Color::new(0.0, 0.0, 0.0);
    pub const BLUE: Color = Color::new(0.0, 0.0, 1.0);
    pub const GREEN: Color = Color::new(0.0, 1.0, 0.0);
    pub const CYAN: Color = Color::new(0.0, 1.0, 1.0);
    pub const RED: Color = Color::new(1.0, 0.0, 0.0);
    pub const MAGENTA: Color = Color::new(1.0, 0.0, 1.0);
    pub const YELLOW: Color = Color::new(1.0, 1.0, 0.0);
    pub const WHITE: Color = Color::new(1.0, 1.0, 1.0);

    pub const fn new(red: f32, green: f32, blue: f32) -> Self {
        Color {
            red,
            green,
            blue
        }
    }

    pub fn to_u32(&self) -> u32 {
        let maximum_intensity = 255.0;
        let red = (self.red * maximum_intensity) as u32;
        let green = (self.green * maximum_intensity) as u32;
        let blue = (self.blue * maximum_intensity) as u32;
        return red << 16 | green << 8 | blue;
    }

    // pub fn average(&self, other: Self) -> Self {
    //     Color {
    //         red: (self.red + other.red) / 2.0,
    //         green: (self.green + other.green) / 2.0,
    //         blue: (self.blue + other.blue) / 2.0
    //     }
    // }
}

impl Mul<f32> for Color {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output {
        Color {
            red: (self.red * rhs).min(1.0),
            green: (self.green * rhs).min(1.0),
            blue: (self.blue * rhs).min(1.0)
        }
    }
}

impl Add<Self> for Color {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Color {
            red: (self.red + rhs.red).min(1.0),
            green: (self.green + rhs.green).min(1.0),
            blue: (self.blue + rhs.blue).min(1.0),
        }
    }
}

impl Div<f32> for Color {
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output {
        return Color {
            red: (self.red / rhs).min(1.0),
            green: (self.green / rhs).min(1.0),
            blue: (self.blue / rhs).min(1.0)
        };
    }
}

impl Display for Color {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "rgb({:.3}, {:.3}, {:.3})", self.red, self.green, self.blue)
    }
}
