use crate::renderer::plane::Rectangle;
use crate::renderer::{Color, Scene, Target};
use crate::Vector;
use super::Size;
use super::Line;
use super::Point;
use std::f32::consts::PI;

#[derive(Copy, Clone)]
pub struct Camera {
    pub position: Point,
    pub size: Size<f32>,
    pub focal_length: f32,
    pub perspective: bool,
    screen: Rectangle,
    focus_point: Point,
}

// TODO: implement freely moving camera (requires matrix transformations)
//   using some sort of lookAt() implementation
impl Camera {
    // Somewhat an emulation of a real camera, the units arbitrarily assumed to be in mm
    // https://en.wikipedia.org/wiki/Full-frame_DSLR
    pub const FULL_FRAME_SIZE: Size<f32> = Size::new(36.0, 24.0);

    // position: the center position of the screen
    //   <---- size.width ----->
    //   *---------------------*                     *
    //   |                     |                     |
    //   |          (p)        |      (f)-----------(p)
    //   |                     |                     |
    //   *---------------------*                     *
    // screen_size: width and height in scene units of the capturing screen. You can compare this somewhat with the "film size".
    // focal_length: the distance behind the camera screen that the focus point lies. Directly impacts the FOV. Compare this with the lens size of a camera (e.g. 50mm) https://en.wikipedia.org/wiki/Camera_lens
    pub fn new(position: Point, screen_size: Size<f32>, focal_length: f32) -> Self {
        // Creates a new camera with an axis aligned screen orthogonal to the z axis and flush with the XY plane (e.g. has Z coordinate set to zero)
        // TODO: This needs to change later, by allowing the camera to move freely in the scene
        //   but that requires some more math, so this is the simplest version.
        //   calculation of screen bounds and focus point should be based on an orientation vector and position, currently assumed to be XY aligned facing Z in the positive direction

        let screen = Self::calculate_screen(position, screen_size);
        let focus_point = Self::calculate_focus_point(position, screen.normal(), focal_length);

        let camera = Self {
            position,
            size: screen_size,
            focal_length,
            perspective: true,
            screen,
            focus_point,
        };
        return camera;
    }

    pub fn capture(&self, scene: &Scene, target: &mut dyn Target) {
        for (pixel, ray) in self.rays(target.size()) {
            for sphere in &scene.spheres {
                let intersection_option = sphere.intersection(&ray);
                if intersection_option.is_none() {
                    continue;
                }
                let (t, intersection_point, intersection_point_surface_normal) = intersection_option.unwrap();
                // println!("pixel ({}, {})", pixel.0, pixel.1);
                // println!("intersection t: {}", t);
                // println!("intersection point: {}", intersection_point);
                // println!("intersection normal: {}", intersection_normal);

                if t < 1.0 {
                    panic!("I think i have detected an intersection that is behind the camera screen, this probably must be culled");
                }

                // Light is now how much the normal faces the Z axis
                // equivalent to a light source emitting from straight in front of the element
                // TODO: calculate from a actual light source with a secondary ray
                let intersection_point_to_light_vector = Vector::between_points(intersection_point, scene.light.position);
                let angle = intersection_point_to_light_vector.angle(intersection_point_surface_normal);
                let brightness = (1.0 - 2.0 * angle / PI).max(0.0);
                // println!("Brightness: {} due to angle {}rad {}deg", brightness, angle, angle * 180.0 / PI);

                let light_color = scene.light.color * brightness;
                let material_color = sphere.color;
                let mut final_color = Color::new(
                    (light_color.red + material_color.red) / 2.0,
                    (light_color.green + material_color.green) / 2.0,
                    (light_color.blue + material_color.blue) / 2.0,
                ) * brightness;

                // if brightness == 0.0 {
                //     final_color = sphere.color / 4.0;
                // }

                // TODO: reverse trace intersection of pixels on screen plane for line wireframe and other types of augmented screen elements (draw sprite, or dots where lights exist etc)

                target.write(pixel, final_color);
            }
        }
    }

    pub fn direction(&self) -> Vector {
        return self.screen.normal();
    }

    pub fn refresh(&mut self) {
        self.screen = Self::calculate_screen(self.position, self.size);
        self.focus_point = Self::calculate_focus_point(self.position, self.screen.normal(), self.focal_length);
    }

    fn rays(&self, samples: Size<u32>) -> RayIterator {
        return RayIterator::new(self, samples);
    }

    fn calculate_focus_point(position: Point, direction: Vector, focal_length: f32) -> Point {
        assert!(focal_length > 0.0, "Focal length must be positive but was {}", focal_length);
        return position + (direction * -focal_length);
    }

    fn calculate_screen(position: Point, size: Size<f32>) -> Rectangle {
        let half_size = size / 2.0;

        let bottom_left_corner = position + Point::new(-half_size.width, -half_size.height, 0.0);
        let top_left_corner = position + Point::new(-half_size.width, half_size.height, 0.0);
        let bottom_right_corner = position + Point::new(half_size.width, -half_size.height, 0.0);

        let screen = Rectangle::new(bottom_left_corner, bottom_right_corner, top_left_corner);

        println!("Recalculating camera screen:");
        println!("\tsize: {}x{}", screen.width(), screen.height());
        println!("\tcorners: bl={} br={} tl={} tr={}", screen.bottom_left(), screen.bottom_right(), screen.top_left(), screen.top_right());
        println!("\tnormal: {}", screen.normal());
        return screen
    }
}

// TODO: values are copied because I don't understand the lifetime concept yet.
pub struct RayIterator {
    counter: u32,
    samples: Size<u32>,
    sample_increment: (f32, f32),
    camera: Camera,
}

impl RayIterator {
    pub fn new(camera: &Camera, samples: Size<u32>) -> Self {
        let sample_increment = (
            camera.screen.width() / (samples.width - 1) as f32,
            camera.screen.height() / (samples.height - 1) as f32
        );

        let it = Self {
            camera: *camera,
            samples,
            counter: 0,
            sample_increment,
        };
        return it;
    }
}

impl Iterator for RayIterator {
    type Item = ((u32, u32), Line);

    // Cast rays from the top left corner of the screen down to the bottom right corner of the screen.
    fn next(&mut self) -> Option<Self::Item> {
        if self.counter == self.samples.width * self.samples.height {
            return None;
        }

        // Rebuild the xy positions based on the counter value
        let x = self.counter % self.samples.width;
        let y = self.counter / self.samples.width;

        let sample_point_on_screen = Point::new(
            self.camera.screen.top_left().x + (self.sample_increment.0 * x as f32),
            self.camera.screen.top_left().y - (self.sample_increment.1 * y as f32),
            0.0
        );

        // Origin of the outgoing ray is behind the screen
        // This will be a perspective camera because all rays cast have the same origin being focus point centered behind the camera screen)
        // The further the focus point is behind the camera, the narrower the FOV, with very large values of z it will approximate more and more an orthogonal projection.
        // with very small values of z the image will look very small (fish eye effect)
        let origin;
        if self.camera.perspective {
            origin = self.camera.focus_point;
        } else {
            origin = sample_point_on_screen + (self.camera.direction() * -self.camera.focal_length);
        }


        // Origin of the outgoing ray is behind the screen
        // This will be an orthogonal camera because all rays cast are parallel (i.e. there is no focal point behind the camera screen)
        //let origin = sample_point_on_screen - Vector::Z_AXIS;

        // Second point of outgoing ray is flush with the camera screen
        //let origin = sample_point_on_screen + Point::new(0.0, 0.0, 1.0);

        let ray = Line::new(origin, sample_point_on_screen);

        // println!("Ray sample #{}: ({}, {}) at point on screen ({})", self.counter, x, y, b);

        self.counter += 1;
        return Some(((x, y), ray));
    }
}
