use super::Vector;
use super::Point;

pub struct Line {
    pub a: Point,
    pub b: Point,

    // Deduced fields precalculated
    v: Vector,
    n: Vector,
}

impl Line {
    pub fn new(a: Point, b: Point) -> Self {
        let v = Vector::between_points(a, b);
        let n = v.normalize();

        return Line {
            a,
            b,
            v,
            n,
        };
    }

    // Normalized direction vector from a -> b
    pub fn direction(&self) -> Vector {
        return self.n;
    }

    pub fn point_on(&self, t: f32) -> Point {
        return self.a + (self.v * t);
    }
}