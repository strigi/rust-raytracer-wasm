use std::collections::HashMap;
use super::Size;
use super::Color;

pub trait Target {
    fn resolution(&self) -> Size<u32>;
    fn size(&self) -> Size<u32>;
    fn clear(&mut self);
    fn write(&mut self, dest: (u32, u32), color: Color);
    fn flip(&mut self);

    fn keydown(&self, key: Key) -> bool;
}

// #[derive(Hash, Eq, PartialEq)]
pub enum Key {
    Up,
    Right,
    Down,
    Left,
    PageUp,
    PageDown,
    UnmappedPlaceholder,
}
