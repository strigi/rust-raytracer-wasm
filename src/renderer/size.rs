use std::fmt::{Display, Formatter};
use std::ops::Div;

#[derive(Copy, Clone)]
pub struct Size<T> {
    pub width: T,
    pub height: T
}

impl<T> Size<T> {
    pub const fn new(width: T, height: T) -> Size<T> {
        return Size {
            width,
            height
        };
    }
}

impl Display for Size<u32> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}x{}", self.width, self.height)
    }
}

impl Display for Size<f32> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:.3}x{:.3}", self.width, self.height)
    }
}

impl Div<u32> for Size<u32> {
    type Output = Self;

    fn div(self, rhs: u32) -> Self::Output {
        return Size::new(self.width / rhs, self.height / rhs);
    }
}

impl Div<f32> for Size<f32> {
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output {
        return Size::new(self.width / rhs, self.height / rhs);
    }
}