use crate::renderer::Color;

#[test]
fn new_creates_color_by_rgb_values() {
    let c = Color::new(0.5, 0.6, 0.7);

    assert_eq!(c.red, 0.5);
    assert_eq!(c.green, 0.6);
    assert_eq!(c.blue, 0.7);
}

#[test]
fn colors_are_added_using_plus_operator_by_color_components() {
    let c = Color::new(0.5, 0.4, 0.7);
    let d = Color::new(0.2, 0.1, 0.3);

    let e = c + d;

    assert_eq!(c.red, 0.5);
    assert_eq!(c.green, 0.4);
    assert_eq!(c.blue, 0.7);

    assert_eq!(d.red, 0.2);
    assert_eq!(d.green, 0.1);
    assert_eq!(d.blue, 0.3);

    assert_eq!(e.red, 0.7);
    assert_eq!(e.green, 0.5);
    assert_eq!(e.blue, 1.0);
}

#[test]
fn color_is_divided_by_scalar_using_divide_operator() {
    let c = Color { red: 0.2, green: 0.4, blue: 0.6 };

    let d = c / 2.0;

    assert_eq!(c.red, 0.2);
    assert_eq!(c.green, 0.4);
    assert_eq!(c.blue, 0.6);

    assert_eq!(d.red, 0.1);
    assert_eq!(d.green, 0.2);
    assert_eq!(d.blue, 0.3);
}