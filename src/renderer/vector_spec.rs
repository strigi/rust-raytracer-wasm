use super::Vector;

#[test]
fn vector_can_be_instantiated_with_coordinates() {
    let v = Vector { x: 1.0, y: 2.0, z: 3.0 };
    assert_eq!(v.x, 1.0);
    assert_eq!(v.y, 2.0);
    assert_eq!(v.z, 3.0);
}

#[test]
fn vectors_can_be_added_without_mutating_operands() {
    let a = Vector { x: 1.0, y: 2.0, z: 3.0 };
    let b = Vector { x: 4.0, y: 5.0, z: 6.0 };

    let c = a + b;

    // left operand is not mutated
    assert_eq!(a.x, 1.0);
    assert_eq!(a.y, 2.0);
    assert_eq!(a.z, 3.0);

    // right operand is not mutated
    assert_eq!(b.x, 4.0);
    assert_eq!(b.y, 5.0);
    assert_eq!(b.z, 6.0);

    // result is sum of both points by coordinates
    assert_eq!(c.x, 5.0);
    assert_eq!(c.y, 7.0);
    assert_eq!(c.z, 9.0);
}

#[test]
fn vectors_can_be_subtracted_without_mutating_operands() {
    let a = Vector { x: 5.0, y: 6.0, z: 7.0 };
    let b = Vector { x: 2.0, y: 1.0, z: 5.0 };

    let c = a - b;

    // left operand is not mutated
    assert_eq!(a.x, 5.0);
    assert_eq!(a.y, 6.0);
    assert_eq!(a.z, 7.0);

    // right operand is not mutated
    assert_eq!(b.x, 2.0);
    assert_eq!(b.y, 1.0);
    assert_eq!(b.z, 5.0);

    // result is sum of both points by coordinates
    assert_eq!(c.x, 3.0);
    assert_eq!(c.y, 5.0);
    assert_eq!(c.z, 2.0);
}

#[test]
fn vector_can_be_multiplied_with_scalar_without_mutating_operands() {
    let v = Vector { x: 1.0, y: 2.0, z: 3.0 };
    let s = 2.0;

    let m = v * s;

    // left operand is not mutated
    assert_eq!(v.x, 1.0);
    assert_eq!(v.y, 2.0);
    assert_eq!(v.z, 3.0);

    // right operand is not mutated
    assert_eq!(s, 2.0);

    // vector is scaled
    assert_eq!(m.x, 2.0);
    assert_eq!(m.y, 4.0);
    assert_eq!(m.z, 6.0);
}

#[test]
fn vector_can_be_divided_by_a_scaler_without_mutating_operands() {
    let v = Vector { x: 1.0, y: 2.0, z: 3.0 };
    let s = 2.0;

    let m = v / s;

    // left operand is not mutated
    assert_eq!(v.x, 1.0);
    assert_eq!(v.y, 2.0);
    assert_eq!(v.z, 3.0);

    // right operand is not mutated
    assert_eq!(s, 2.0);

    // vector is scaled
    assert_eq!(m.x, 0.5);
    assert_eq!(m.y, 1.0);
    assert_eq!(m.z, 1.5);
}

#[test]
fn vector_multiplied_by_vector_with_star_operator_is_cross_product() {
    let a = Vector { x: 1.0, y: 3.0, z: -5.0 };
    let b = Vector { x: 4.0, y: -2.0, z: -1.0 };

    let c = a.cross(b);
    let d = a * b;

    assert_eq!(a.x, 1.0);
    assert_eq!(a.y, 3.0);
    assert_eq!(a.z, -5.0);

    assert_eq!(b.x, 4.0);
    assert_eq!(b.y, -2.0);
    assert_eq!(b.z, -1.0);

    assert_eq!(c.x, -13.0);
    assert_eq!(c.y, -19.0);
    assert_eq!(c.z, -14.0);

    assert_eq!(c.x, d.x);
    assert_eq!(c.y, d.y);
    assert_eq!(c.z, d.z);
}

#[test]
fn vector_length_returns_the_magnitude_of_the_vector() {
    let v = Vector::new(1.0, -2.0, 3.0);

    assert_eq!(v.x, 1.0);
    assert_eq!(v.y, -2.0);
    assert_eq!(v.z, 3.0);

    assert_eq!(v.length(), 3.7416575);
}

#[test]
fn vector_normalize_results_in_a_vector_of_length_one_in_the_same_direction() {
    let v = Vector::new(3.0, 1.0, 2.0);

    let n = v.normalize();

    assert_eq!(v.x, 3.0);
    assert_eq!(v.y, 1.0);
    assert_eq!(v.z, 2.0);

    assert_eq!(n.x, 0.8017837);
    assert_eq!(n.y, 0.26726124);
    assert_eq!(n.z, 0.5345225);

    assert!(n.length() - 1.0 < 0.000001);
}

#[test]
fn vector_multiplied_by_vector_is_dot_product_when_type_assigned_to_is_vector() {
    let a = Vector { x: 1.0, y: 3.0, z: -5.0 };
    let b = Vector { x: 4.0, y: -2.0, z: -1.0 };

    let c = a.dot(b);

    // left operand is not mutated
    assert_eq!(a.x, 1.0);
    assert_eq!(a.y, 3.0);
    assert_eq!(a.z, -5.0);

    // right operand is not mutated
    assert_eq!(b.x, 4.0);
    assert_eq!(b.y, -2.0);
    assert_eq!(b.z, -1.0);

    assert_eq!(c, 3.0);
}