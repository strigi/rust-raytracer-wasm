use super::*;

struct Triangle {
    a: Point,
    b: Point,
    c: Point
}

impl Triangle {
    pub fn new(a: Point, b: Point, c: Point) -> Self {
        return Triangle { a, b, c };
    }
}