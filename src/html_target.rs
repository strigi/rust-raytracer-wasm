use js_sys::JsString;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{HtmlCanvasElement, Window, CanvasRenderingContext2d, console, ImageData};
use crate::renderer::{Color, Size, Target, Key};

pub struct HTMLTarget {
    canvas: HtmlCanvasElement,
    context: CanvasRenderingContext2d,
    scale: u32,
}

impl HTMLTarget {
    pub fn from_id(canvas_id: &str, scale: u32) -> Self {
        log(&format!("Creating rendering target using canvas '#{}'", canvas_id));

        let window = web_sys::window().unwrap();

        let document = window.document().unwrap();
        let canvas = document.get_element_by_id(canvas_id).unwrap();
        let canvas: web_sys::HtmlCanvasElement = canvas.dyn_into::<HtmlCanvasElement>().map_err(|_| ()).unwrap();

        let context = canvas.get_context("2d").unwrap().unwrap().dyn_into::<CanvasRenderingContext2d>().unwrap();

        // ImageData(width, height)
//        new ImageData(width, height);

        // context.begin_path();
        // context.move_to(100.0, 100.0);
        // context.line_to(200.0, 200.0);
        // context.stroke();

        return Self {
            canvas,
            context,
            scale,
        };
    }

    fn width_f64(&self) -> f64 {
        return self.resolution().width as f64;
    }

    fn height_f64(&self) -> f64 {
        return self.resolution().height as f64;
    }
}

impl Target for HTMLTarget {
    fn resolution(&self) -> Size<u32> {
        let resolution = Size::new(self.canvas.width(), self.canvas.height());
        log(&format!("HTMLTarget::resolution() -> {}", resolution));
        return resolution;
    }

    fn size(&self) -> Size<u32> {
        let size = self.resolution() / self.scale;
        log(&format!("HTMLTarget::size() -> {}", size));
        return size;
    }

    fn clear(&mut self) {
        // self.context.set_fill_style(&"rgb(0, 0, 0)".into());
        self.context.set_fill_style(&"rgb(0, 0, 255)".into());
        self.context.fill_rect(0.0, 0.0, self.width_f64(), self.height_f64());
        log(&format!("HTMLTarget::clear()"));
    }

    // TODO: work with image_data pixels and scale with draw_image()
    fn write(&mut self, dest: (u32, u32), color: Color) {
        // log(&format!("HTMLTarget::write(({}, {}), {})", dest.0, dest.1, color));
        // let image_data = self.context.get_image_data(0.0, 0.0, self.width_f64(), self.height_f64()).unwrap();
        // let mut data = image_data.data();
        // let x = dest.0;
        // let y = dest.1;
        //
        // let i = (y * self.resolution().width + x) as usize;
        // log(&format!("PPP {}", i));
        // data[i + 0] = 0xAA;
        // data[i + 1] = 0xAA;
        // data[i + 2] = 0xAA;
        // data[i + 3] = 0xAA;

        let res = self.resolution();
        let size = self.size();
        let scale = self.scale as f64;
        let x = dest.0 as f64;
        let y = dest.1 as f64;

        let red = (color.red * 255.0) as u32;
        let green = (color.green * 255.0) as u32;
        let blue = (color.blue * 255.0) as u32;

        let val = format!("rgb({}, {}, {})", red, green, blue);
        log(&format!("Color converted from '{}' to: '{}'", color, val));
        self.context.set_fill_style(&val.into());
        self.context.fill_rect(
            x * scale,
            y * scale,
            scale as f64,
            scale as f64
        );
    }

    fn flip(&mut self) {
        log(&format!("HTMLTarget::flip()"));
    }

    fn keydown(&self, key: Key) -> bool {
        println!("HTML key mapping todo");
        return false;
    }
}

fn log(s: &str) {
    console::log_1(&s.into());
}