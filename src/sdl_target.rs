use std::borrow::BorrowMut;
use std::collections::HashMap;
use std::iter::Scan;
use std::rc::Rc;
use sdl2::pixels::{PixelFormat, PixelFormatEnum};
use sdl2::event::Event;
use sdl2::keyboard::{KeyboardState, Keycode, Scancode};
use sdl2::EventPump;
use sdl2::rect::Rect;
use sdl2::render::WindowCanvas;
use crate::{Key, Size};
use crate::Color;
use crate::Target;

pub struct SDLTarget {
    canvas: WindowCanvas,
    event_pump: EventPump,
    pixel_format: PixelFormat,
}

pub struct KeyMap {
    map: HashMap<Key, bool>,
}

// impl KeyMap {
//     fn new() -> Self {
//         return Self {
//             map: HashMap::new(),
//         }
//     }
//
//     pub fn get(&self, key: Key) -> bool {
//         return self.map.contains_key(&key);
//     }
//
//     fn update_from_sdl(&mut self, sdl_keyboard_state: &KeyboardState) {
//         self.map.clear();
//         for sdl_scancode in sdl_keyboard_state.pressed_scancodes() {
//             let key = KeyMap::convert(sdl_scancode);
//             self.map.insert(key, true);
//         }
//     }
//
//     fn convert(sdl_scancode: Scancode) -> Key {
//         return match sdl_scancode {
//             Scancode::Up => { Key::Up }
//             Scancode::Right => { Key::Right }
//             Scancode::Down => { Key::Down }
//             Scancode::Left => { Key::Left }
//             Scancode::PageUp => { Key::PageUp }
//             Scancode::PageDown => { Key::PageDown }
//             _ => { Key::UnmappedPlaceholder }
//         }
//     }
// }

impl SDLTarget {
    pub fn new(width: u32, height: u32, scale: u32, fullscreen: bool) -> Self {
        let context = sdl2::init().unwrap();

        let video = context.video().unwrap();

        let mut window_builder = video.window("Eothel's Raytracer: SDL Target", width, height);
        window_builder.position_centered();
        if fullscreen {
            window_builder.fullscreen();
        }
        let window = window_builder.build().unwrap();

        let mut canvas = window.into_canvas().build().unwrap();
        canvas.set_scale(scale as f32, scale as f32).unwrap();

        let event_pump = context.event_pump().unwrap();

        return Self {
            canvas,
            event_pump,
            pixel_format: PixelFormat::try_from(PixelFormatEnum::ARGB8888).unwrap(),
        };
    }

    pub fn handle_events(&mut self) -> bool {
        for event in self.event_pump.poll_iter() {
            match event {
                Event::Quit {..} => {
                    println!("Quit");
                    return false;
                }

                _ => {
                }
            }
        }
        return true;
    }
}

impl Target for SDLTarget {
    fn resolution(&self) -> Size<u32> {
        let (width, height) = self.canvas.window().size();
        return Size::new(width, height);
    }

    fn size(&self) -> Size<u32> {
        let scale = self.canvas.scale().0 as u32;
        return self.resolution() / scale;
    }

    fn clear(&mut self) {
        self.canvas.set_draw_color(sdl2::pixels::Color::RGB(0, 0, 0));
        // self.canvas.set_draw_color(sdl2::pixels::Color::RGB(0, 0, 255));
        self.canvas.clear();
    }

    fn write(&mut self, dest: (u32, u32), color: Color) {
        self.canvas.set_draw_color(sdl2::pixels::Color::from_u32(&self.pixel_format, color.to_u32()));
        self.canvas.draw_point((dest.0 as i32, dest.1 as i32)).unwrap();
    }

    fn flip(&mut self) {
        self.canvas.present();
    }

    fn keydown(&self, key: Key) -> bool {
        let sdl_scan_code = match key {
            Key::Up => { Scancode::Up }
            Key::Down => { Scancode::Down }
            Key::Left => { Scancode::Left }
            Key::Right => { Scancode::Right }
            Key::PageUp => { Scancode::PageUp }
            Key::PageDown => { Scancode::PageDown }
            _ => {
                return false;
            }
        };
        return self.event_pump.keyboard_state().is_scancode_pressed(sdl_scan_code);

        //     fn update_from_sdl(&mut self, sdl_keyboard_state: &KeyboardState) {
//         self.map.clear();
//         for sdl_scancode in sdl_keyboard_state.pressed_scancodes() {
//             let key = KeyMap::convert(sdl_scancode);
//             self.map.insert(key, true);
//         }
//     }
//
//     fn convert(sdl_scancode: Scancode) -> Key {
//         return match sdl_scancode {
//             Scancode::Up => { Key::Up }
//             Scancode::Right => { Key::Right }
//             Scancode::Down => { Key::Down }
//             Scancode::Left => { Key::Left }
//             Scancode::PageUp => { Key::PageUp }
//             Scancode::PageDown => { Key::PageDown }
//             _ => { Key::UnmappedPlaceholder }
//         }
//     }
    }
}