mod renderer;
mod sdl_target;

use std::borrow::BorrowMut;
use std::thread::sleep;
use std::time::{Duration, SystemTime};
use renderer::*;
use sdl_target::*;

pub fn main() {
    let mut t = SDLTarget::new(960, 540, 5, false);
    let mut r = Renderer::new();

    while t.handle_events() {
        if t.keydown(Key::Left) {
            r.scene.light.position.x -= 0.1;
        }
        if t.keydown(Key::Right) {
            r.scene.light.position.x += 0.1;
        }

        if t.keydown(Key::PageUp) {
            r.scene.light.position.y += 0.1;
        }
        if t.keydown(Key::PageDown) {
            r.scene.light.position.y -= 0.1;
        }

        if t.keydown(Key::Up) {
            r.scene.light.position.z += 0.1;
        }
        if t.keydown(Key::Down) {
            r.scene.light.position.z -= 0.1;
        }
        println!("Light: {}", r.scene.light.position);

        r.draw(&mut t);
        //sleep(Duration::from_millis(1000 / 60));
    }
}
